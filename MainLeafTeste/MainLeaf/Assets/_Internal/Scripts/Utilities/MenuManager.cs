﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MenuManager : MonoBehaviour
{
    [SerializeField] private int _loadScene;
    [SerializeField] private GameObject _loadPanel;

    public void NewGame()
    {
        GameManager.ResetSaveEvent?.Invoke();
        Change();
    }

    public void ContinueGame()
    {
        Change();
    }

    private void Change()
    {
        _loadPanel.SetActive(true);
        StartCoroutine(LoadYourAsyncScene());
    }
    IEnumerator LoadYourAsyncScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(_loadScene);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
