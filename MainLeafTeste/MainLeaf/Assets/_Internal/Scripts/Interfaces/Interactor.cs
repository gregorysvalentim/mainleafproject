public interface Iinteractor
{
    void OnDragObject();
    void OffDragObject();
    void OnMoving();
    void OffMoving();
}