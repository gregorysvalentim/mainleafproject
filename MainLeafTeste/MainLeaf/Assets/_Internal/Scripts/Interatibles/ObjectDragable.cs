using System.Collections;
using UnityEngine;

public class ObjectDragable : MonoBehaviour, Iinteratibles
{
    [SerializeField] private Rigidbody _rigidBody;
    [SerializeField] private LayerMask _layerCheckIfCanMove;
    [SerializeField] private Vector3 _offSet;
    [SerializeField] private Vector3 _targetPosition;
    [SerializeField] private bool _canInteract = true;
    [SerializeField] private bool _inMovement;
    [SerializeField] private int _moveBoxDistance = 2;
    [SerializeField] private Vector3 _objectivePosition;
    [SerializeField] private string _uIactionName;
    public string ActionName => _uIactionName;
    public bool CanInteract => _canInteract;
    public GrabController Graber { get; set; }

    public delegate void PuzzleSolvedHandler();
    public static PuzzleSolvedHandler PuzzleSolvedEvent;

    private void OnEnable()
    {
        PuzzleSolvedEvent += BoxPosition;
    }

    private void OnDisable()
    {
        PuzzleSolvedEvent -= BoxPosition;
    }

    private void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
    }

    private void BoxPosition()
    {
            transform.position = _objectivePosition;
    }

    private bool CheckIfCanMove(Vector3 direction)
    {
        
        return Physics.Raycast(transform.position + _offSet + direction, direction, _moveBoxDistance, _layerCheckIfCanMove);
    }

    private IEnumerator MovingObject()
    {
        yield return null;
        if (_inMovement == true && !transform.position.CheckingApproximately(_targetPosition, 0.1f))
        {
            transform.position = Vector3.MoveTowards(transform.position, _targetPosition, 0.02f);
            IEnumerator Moving = MovingObject();
            StartCoroutine(Moving);
        }
        else
        {
            _inMovement = false;
            transform.position = _targetPosition;
        }
    }
        
    public void MoveObject(Vector3 axis, GrabController grabController)
    {
        if (!CheckIfCanMove(axis))
        {
            _targetPosition = transform.position + (axis);
            _targetPosition = new Vector3(Mathf.RoundToInt(_targetPosition.x), transform.position.y, Mathf.RoundToInt(_targetPosition.z));
            _inMovement = true;
            IEnumerator Moving = MovingObject();
            StartCoroutine(Moving);
            grabController.OnMoving();
        }
    }

    public void OnDragObject()
    {
        _rigidBody.constraints = RigidbodyConstraints.None;
        _rigidBody.constraints = RigidbodyConstraints.FreezeRotation;
    }

    public void OffDragObject()
    {
        _rigidBody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "BoxTrigger")
        {
            OffDragObject();
            _canInteract = false;
            Graber.OffDragObject();
        }
    }
}
