﻿using UnityEngine;
using UnityEngine.AI;
using System.Threading.Tasks;
public class EnemyFixedPath : MonoBehaviour
{
    [SerializeField] private Vector3[] _positions;
    [SerializeField] private int _indexPositins = 0;
    [SerializeField] private int _timeToUpdateIndex = 4000;
    private NavMeshAgent _navMeshAgent;
    private Animator _animator;
    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
        UpdateIndex();
        UpdateDestination();
    }
    private void Update()
    {
        if(transform.position.CheckingApproximately(_positions[_indexPositins],2f))
        {
            _navMeshAgent.isStopped = true;
            UpdateIndex();
            UpdateDestination();
        }
    }
    private async void UpdateDestination()
    {
        _animator.SetBool("Movement", false);
        await Task.Delay(_timeToUpdateIndex);
        _navMeshAgent.isStopped = false;
        _navMeshAgent.SetDestination(_positions[_indexPositins]);
        _animator.SetBool("Movement", true);
    }
    private void UpdateIndex()
    {
        _indexPositins++;
        if(_indexPositins >= _positions.Length)
        {
            _indexPositins = 0;
        }
    }
}
