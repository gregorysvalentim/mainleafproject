using UnityEngine;
public class ChangeSceneInTrigger : MonoBehaviour
{
    [SerializeField] private int _sceneInt;
    public void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.TryGetComponent(out MovementController t))
        {
            GameManager.AddPuzzleLevelEvent?.Invoke(_sceneInt);
            UIManager.LoadSceneEvent?.Invoke(_sceneInt);
        }
    }
}
