using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class UIManager : MonoBehaviour
{
    [SerializeField] private TMP_Text _coinText;
    [SerializeField] private TMP_Text _actionText;
    [SerializeField] private GameObject _losePanel;
    [SerializeField] private GameObject _loadPanel;
    [SerializeField] private GameObject _pausePanel;
    public delegate void ChangeActionTextHandler(string action);
    public static ChangeActionTextHandler ChangeActionTextEvent;
    public delegate void AddCoinTextHandler(int coin);
    public static AddCoinTextHandler AddCoinTextEvent;
    public delegate void LosePanelHandler();
    public static LosePanelHandler LosePanelEvent;
    public delegate void PausePanelHandler();
    public static PausePanelHandler PausePanelEvent;
    public delegate void LoadSceneHandler(int scene);
    public static LoadSceneHandler LoadSceneEvent;

    private void OnEnable()
    {
        AddCoinTextEvent += AddCoin;
        ChangeActionTextEvent += GrabActionText;
        LosePanelEvent += LosePanelActive;
        LoadSceneEvent += LoadScene;
        PausePanelEvent += PausePanelActive;
    }

    private void OnDisable()
    {
        AddCoinTextEvent -= AddCoin;
        ChangeActionTextEvent -= GrabActionText;
        LosePanelEvent -= LosePanelActive;
        LoadSceneEvent -= LoadScene;
        PausePanelEvent -= PausePanelActive;
    }

    private void LosePanelActive()
    {
        _losePanel.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    private void LoadScene(int scene)
    {
        _loadPanel.SetActive(true);
        StartCoroutine(LoadYourAsyncScene(scene));
    }

    private void AddCoin(int coin)
    {
        int coins = int.Parse(_coinText.text) + coin;
        _coinText.text = coins.ToString();
    }

    public void PausePanelActive()
    {
        if(_pausePanel.activeInHierarchy == false)
        {
            _pausePanel.SetActive(true);
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            _pausePanel.SetActive(false);
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    private void GrabActionText(string action)
    {
        _actionText.text = action;
    }

    List<AsyncOperation> LoadScenes = new List<AsyncOperation>();
    IEnumerator LoadYourAsyncScene(int scene)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(scene);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
